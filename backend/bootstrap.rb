require './lib/notification/email'

require './models/customer/address'
require './models/customer/customer'

require './models/order/order_item'
require './models/order/order'

require './models/product/product'
require './models/product/book'
require './models/product/digital'
require './models/product/physical'
require './models/product/membership'

require './models/payment/credit_card'
require './models/payment/invoice'
require './models/payment/payment'

# Book Example (build new payments if you need to properly test it)
foolano = Customer.new(name: 'Candido', email: 'ca@gmail.com')
book = Book.new(name: 'Awesome book')
book_order = Order.new(foolano)
book_order.add_product(book)

payment_book = Payment.new(order: book_order,
                           payment_method: CreditCard.new(code: '43567890'))
payment_book.pay
p payment_book.paid? # < true
p payment_book.order.items.first.product.type

payment_book.prepare_notifications

# now, how to deal with shipping rules then?

# shipping_label in order
