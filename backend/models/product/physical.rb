# Physical
class Physical < Product
  attr_accessor :shipping_label

  def initialize(args)
    @shipping_label = args[:shipping_label]
    super(args)
  end

  def after_payment(args)
    prepare_shipping_label(args)
  end

  def prepare_shipping_label(args)
    order = args.fetch(:order, nil)
    return if order.nil?
    @shipping_label = ''
    @shipping_label += "CUSTOMER: #{order.customer.name} "
    @shipping_label += "(#{order.customer.email}) - "
    @shipping_label += "ZIPCODE: #{order.address.zipcode}"
  end

  def type
    'physical'
  end
end
