# Membership
class Membership < Product
  include Notification::Email

  attr_reader :customer

  def initialize(args)
    @customer = args[:customer]
    super(args)
  end

  def after_payment(args)
    send_email(args)
    active_subscription(args)
  end

  def active_subscription(args)
    order = args.fetch(:order, nil)
    customer = order.customer unless order.nil?
    customer&.subscription = true
    puts "Enable subscription to #{customer.email}" if customer.subscription == true
  end

  def send_email(args)
    Notification::Email.send(args)
  end

  def type
    'membership'
  end
end
