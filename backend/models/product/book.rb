# Book
class Book < Product
  attr_reader :shipping_label

  def initialize(args)
    @shipping_label = args[:shipping_label]
    super(args)
  end

  def after_payment(_args)
    prepare_shipping_label
  end

  def prepare_shipping_label
    @shipping_label = 'Tax free as provided in the Const. Art. 150, VI, d'
    puts @shipping_label
  end

  def type
    'book'
  end
end
