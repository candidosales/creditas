# Product
class Product
  attr_reader :name

  def initialize(args)
    @name = args[:name]
  end

  def after_payment
    raise NotImplementedError, "This #{self.class} cannot respond to:"
  end

  def type
    raise NotImplementedError, "This #{self.class} cannot respond to:"
  end
end
