# Digital
class Digital < Product
  include Notification::Email
  attr_reader :voucher

  def initialize(args)
    @voucher = args.fetch(:voucher, 10)
    super(args)
  end

  def after_payment(args)
    send_email(args)
    add_voucher(args)
  end

  def send_email(args)
    Notification::Email.send(args)
  end

  def add_voucher(args)
    order = args.fetch(:order, nil)
    customer = order.customer unless order.nil?
    customer.voucher += @voucher unless customer.nil?
    puts "Add voucher to #{customer.email} of R$ #{@voucher}" unless customer.nil?
  end

  def type
    'digital'
  end

  def body_email(args)
    order = args.fetch(:order, nil)
    return unless order.nil?
    message = 'Order: '
    order.items.each do |item|
      message += "Item: #{item.product.name} - R$ #{item.total}"
    end
    message += "Total: #{order.total_amount}"
  end
end
