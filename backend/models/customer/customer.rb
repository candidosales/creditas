# Customer
class Customer
  attr_accessor :name, :email, :voucher, :subscription
  def initialize(args)
    @name = args[:name]
    @email = args[:email]
    @voucher = args.fetch(:voucher, 0)
    @subscription = args.fetch(:subscription, false)
  end
end
