class Notification
  module Email
    def self.send(args)
      customer = args.fetch(:customer, nil)
      body = args.fetch(:body, '')
      "Send email to #{customer.email} with #{body}" unless customer.nil?
    rescue => exception
      puts "Error Notification::Email, #{exception.message}"
    end
  end
end
