# Desafio para Backend Software Engineer 

__Explicação__

## 1. Organização

- Separação do código que estava em um arquivo, em suas respectivas pastas;

## 2. Hierarquia

- Percebe-se a repetição de atributos/responsabilidades entre os tipos de produtos (compartilham os mesmos atributos/comportamentos), com isso foi criado um objeto `Product` que será a classe mãe dos tipos de produtos, isso facilita a inclusão de novos tipos à longo prazo herdando características em comum;
- Todos os produtos possuem um comportamento diferente após o fluxo de pagamento, com isso todos implementam `after_payment` para atender este recurso;
  - Na classe `Payment` implementa o método `prepare_notifications` que é executado após o pagamento. Nele se analise todos os itens do pedido e verifica se é implementado método `after_payment` para o mesmo ser executado, isso evita o Duck Typing caso surja novos tipos de produto;

## 3. Módulos

- Para o serviço de envio de e-mail foi criado um módulo que pode ser compartilhado por qualquer tipo de objeto na aplicação;

## 4. Qualidade

### 4.1 Testes

Existem dois testes:

- Teste de envio de e-mail:

```ruby
ruby test/lib/notification_email_spec.rb -v
```

- Teste de pagamentos para cada tipo de produto:

```ruby
ruby test/models/payment/payment_spec.rb -v
```

### 4.2 Análise estática

```ruby
rubocop
```

# Contato

* Cândido Sales Gomes : [linkedin](https://www.linkedin.com/in/candidosales) | [twitter](https://twitter.com/candidosales) | [github](https://github.com/candidosales) | [email](mailto:candidosg@gmail.com)

# Copyright

Released under MIT License.

Copyright © 2017-2017 Cândido Sales Gomes.