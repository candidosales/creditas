require './test/test_helper'
require './models/customer/customer'
require './lib/notification/email'

describe 'Notification Email' do
  before do
    @customer = Customer.new(name: 'Candido', email: 'candidosg@gmail.com')
  end

  describe 'when create a notification to email' do
    it 'send email to an existing customer' do
      respond = Notification::Email.send(customer: @customer)
      respond.must_equal "Send email to #{@customer.email} with "
    end

    it 'when customer is nil dont send email' do
      respond = Notification::Email.send(customer: nil)
      respond.must_be_nil
    end
  end
end
