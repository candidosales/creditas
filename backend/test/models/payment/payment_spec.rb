require './test/test_helper'

require './lib/notification/email'

require './models/customer/customer'
require './models/customer/address'

require './models/order/order'
require './models/order/order_item'

require './models/payment/payment'
require './models/payment/credit_card'
require './models/payment/invoice'

require './models/product/product'
require './models/product/book'
require './models/product/digital'
require './models/product/physical'
require './models/product/membership'

describe 'Payment' do
  before do
    @customer = Customer.new(name: 'Candido', email: 'candidosg@gmail.com')
    @order = Order.new(@customer)
  end

  describe 'when buy a physical' do
    before do
      @physical = Physical.new(name: 'Awesome physical')
      @order.add_product(@physical)
      @payment_physical = Payment.new(order: @order,
                                      payment_method: CreditCard.new(code: '43567890'))
    end

    it 'generate shipping label to place in shipping box' do
      @payment_physical.pay

      shipping_label = ''
      shipping_label += "CUSTOMER: #{@order.customer.name} "
      shipping_label += "(#{@order.customer.email}) - "
      shipping_label += "ZIPCODE: #{@order.address.zipcode}"

      @physical.shipping_label.must_equal shipping_label
    end
  end

  describe 'when buy a membership' do
    before do
      @membership = Membership.new(name: 'Awesome membership')
      @order.add_product(@membership)
      @payment_membership = Payment.new(order: @order,
                                        payment_method: CreditCard.new(code: '43567890'))
    end

    it 'send notification with message and active subscription' do
      @payment_membership.pay
      @customer.subscription.must_equal true
    end
  end

  describe 'when buy a book' do
    before do
      @book = Book.new(name: 'Awesome book')
      @order.add_product(@book)
      @payment_book = Payment.new(order: @order,
                                  payment_method: CreditCard.new(code: '43567890'))
    end

    it 'send notification with message of tax free' do
      @payment_book.pay
      @book.shipping_label.must_equal 'Tax free as provided in the Const. Art. 150, VI, d'
    end
  end

  describe 'when buy a digital' do
    before do
      @digital = Digital.new(name: 'Awesome digital')
      @order.add_product(@digital)
      @payment_digital = Payment.new(order: @order,
                                     payment_method: CreditCard.new(code: '43567890'))
    end

    it 'send notification and add voucher to customer' do
      @payment_digital.pay
      @customer.voucher.must_equal @digital.voucher
    end
  end
end
