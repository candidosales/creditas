const Chat = (function() {

    const messages = document.querySelectorAll('.messages'),
        queryInput = '.message-input input',
        queryButton = '.message-input button',
        queryMessageList = '.message-list';

    function init() {
        console.log('init');
        fetchElements();
    }

    function fetchElements() {
        for (var i = 0; i < messages.length; i++) {
            const button = getButtonByIndex(i);
            onSubmitMessage(button, i);
        }
    }

    function onSubmitMessage(button, index) {
        const input = messages[index].querySelector(queryInput);

        button.addEventListener('click', function() {
            if (input.value !== null && input.value !== '') {
                addMessage(input.value, index);
                input.value = '';
            }
        });
    }

    function getTemplateMessage(message) {
        if (message !== null && message !== '') {
            let messageTemplate = document.createElement('li');
            messageTemplate.className = 'message-item';
            messageTemplate.innerHTML = message;

            return messageTemplate;
        }
    }

    function addMessage(message, index) {
        const messageList = getMessageListByIndex(index),
            template = getTemplateMessage(message);

        if (messageList !== null && messageList !== undefined) {
            messageList.appendChild(template);
        }
    }

    function getButtonByIndex(index) {
        return messages[index].querySelector(queryButton);
    }

    function getMessageListByIndex(index) {
        return messages[index].querySelector(queryMessageList);
    }

    return {
        init: init,
        getButtonByIndex: getButtonByIndex,
        getMessageListByIndex: getMessageListByIndex,
        addMessage: addMessage
    }

})();

Chat.init();