const ChatWebRtc = (function(_chat) {

    let video = null,
        chat = _chat,
        index = 0,
        errorElement = null,
        buttonVideo = null,
        MediaStream = null;

    const queryButton = '.button-video',
        queryVideo = '.video';

    let constraints = window.constraints = {
        audio: false,
        video: true
    };

    function init(_index) {
        index = _index || 0;

        addComponentsVideo();
    }

    function addComponentsVideo() {
        addVideo();
        addButtonStartVideo();
    }

    function addVideo() {
        let messageList = chat.getMessageListByIndex(index);
        messageList.insertAdjacentHTML('afterend', '<video id="video-' + index + '"></video><div id="errorMsg"></div>');
        errorElement = document.querySelector('#errorMsg');
    }

    function addButtonStartVideo() {
        let button = chat.getButtonByIndex(index);
        let idStartHtml = 'button-video-start-' + index;
        let idStopHtml = 'button-video-stop-' + index;

        button.insertAdjacentHTML('afterend', '<button id="' + idStopHtml + '" target="video-' + index + '">Parar Vídeo</button>');
        button.insertAdjacentHTML('afterend', '<button id="' + idStartHtml + '" target="video-' + index + '">Iniciar Vídeo</button>');

        buttonStartVideo = document.querySelector('#' + idStartHtml);
        onStartVideo(buttonStartVideo);

        buttonStopVideo = document.querySelector('#' + idStopHtml);
        onStopVideo(buttonStopVideo);
    }

    function onStartVideo(button) {
        button.addEventListener('click', function() {
            video = document.querySelector('#' + button.getAttribute('target'));
            startVideo();
        });
    }

    function onStopVideo(button) {
        button.addEventListener('click', function() {
            video = document.querySelector('#' + button.getAttribute('target'));
            stopVideo();
        });
    }


    // Put variables in global scope to make them available to the browser console.
    function handleSuccess(stream) {
        const videoTracks = stream.getVideoTracks();
        console.log('Got stream with constraints:', constraints);
        console.log('Using video device: ' + videoTracks[0].label);
        stream.oninactive = function() {
            console.log('Stream inactive');
        };
        window.stream = stream; // make variable available to browser console
        video.srcObject = stream;
        MediaStream = stream.getTracks()[0];
    }

    function handleError(error) {
        if (error.name === 'ConstraintNotSatisfiedError') {
            errorMsg('The resolution ' + constraints.video.width.exact + 'x' +
                constraints.video.width.exact + ' px is not supported by your device.');
        } else if (error.name === 'PermissionDeniedError') {
            errorMsg('Permissions have not been granted to use your camera and ' +
                'microphone, you need to allow the page access to your devices in ' +
                'order for the demo to work.');
        }
        errorMsg('getUserMedia error: ' + error.name, error);
    }

    function errorMsg(msg, error) {
        errorElement.innerHTML += '<p>' + msg + '</p>';
        if (typeof error !== 'undefined') {
            console.error(error);
        }
    }

    function startVideo() {
        navigator.mediaDevices
            .getUserMedia(constraints)
            .then(handleSuccess)
            .catch(handleError);
    }

    function stopVideo() {
        if (MediaStream !== null && MediaStream !== undefined) {
            MediaStream.stop();
        }
    }


    return {
        init: init,
        addComponentsVideo: addComponentsVideo
    }
})(Chat);

ChatWebRtc.init(1);