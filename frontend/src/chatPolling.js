const ChatPolling = (function(_chat) {

    let chat = _chat;
    let index = 0;

    function init(_index) {
        index = _index || 0;
        poll();
    }

    // Just for the example, it is recommended to use socket.io
    function poll() {
        setTimeout(function() {
            ajaxRequest(index);
        }, 2500);
    }

    function ajaxRequest(index) {
        const response = JSON.parse('{ "message": "teste 1"}');
        chat.addMessage(response.message, index);
        poll(index);
    }

    return {
        init: init,
        poll: poll
    }
})(Chat);

ChatPolling.init(1);